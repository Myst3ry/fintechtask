package com.myst3ry.data.mapper;

import com.myst3ry.data.source.remote.pojo.courses.CourseData;
import com.myst3ry.domain.model.Course;
import com.myst3ry.domain.utils.ConvertUtils;

import java.util.ArrayList;
import java.util.List;

public class CourseDataMapper {

    private CourseDataMapper() { }

    public static Course transform(final CourseData courseData) {
        return new Course(
                ConvertUtils.parseDate(courseData.getEventDateStart()),
                courseData.getTitle(),
                courseData.getUrl(),
                courseData.getStatus()
        );
    }

    public static List<Course> transform(final List<CourseData> courseDataList) {
        final List<Course> courses = new ArrayList<>();
        for (final CourseData courseData : courseDataList) {
            courses.add(transform(courseData));
        }
        return courses;
    }
}
