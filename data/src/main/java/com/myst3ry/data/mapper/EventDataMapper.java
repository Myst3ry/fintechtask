package com.myst3ry.data.mapper;

import com.myst3ry.data.source.remote.pojo.events.EventData;
import com.myst3ry.domain.model.Event;
import com.myst3ry.domain.utils.ConvertUtils;

import java.util.ArrayList;
import java.util.List;

public class EventDataMapper {

    private EventDataMapper() { }

    public static Event transform(final EventData eventData) {
        return new Event(
                eventData.getTitle(),
                ConvertUtils.formatEventDate(eventData.getDateStart(), eventData.getDateEnd()),
                eventData.getPlace(),
                eventData.getDescription()
        );
    }

    public static List<Event> transform(final List<EventData> eventDataList) {
        final List<Event> events = new ArrayList<>();
        for (final EventData eventData : eventDataList) {
            events.add(transform(eventData));
        }
        return events;
    }
}
