package com.myst3ry.data.mapper;

import com.myst3ry.data.source.remote.pojo.grades.GradeData;
import com.myst3ry.domain.model.Grade;

import java.util.ArrayList;
import java.util.List;

public class GradeDataMapper {

    private GradeDataMapper() { }

    public static Grade transform(final GradeData gradeData) {
        return new Grade();
    }

    public static List<Grade> transform(final List<GradeData> gradeDataList) {
        final List<Grade> grades = new ArrayList<>();
        for (final GradeData gradeData : gradeDataList) {
            grades.add(transform(gradeData));
        }
        return grades;
    }
}
