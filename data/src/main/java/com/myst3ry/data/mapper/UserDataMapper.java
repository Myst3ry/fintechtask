package com.myst3ry.data.mapper;

import com.myst3ry.data.source.remote.api.FintechApi;
import com.myst3ry.data.source.remote.pojo.user.UserData;
import com.myst3ry.domain.model.User;

public class UserDataMapper {

    private UserDataMapper() { }

    public static User transform(final UserData userData) {
        return new User(
                userData.getId(),
                userData.getFirstName(),
                userData.getLastName(),
                userData.getMiddleName(),
                userData.getBirthday(),
                FintechApi.BASE_URL + userData.getAvatar(),
                userData.getEmail(),
                userData.getDepartment(),
                userData.getUniversity(),
                userData.getFaculty(),
                userData.getRegion()
        );
    }
}
