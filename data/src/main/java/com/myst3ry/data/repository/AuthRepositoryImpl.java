package com.myst3ry.data.repository;

import android.text.TextUtils;

import com.myst3ry.data.source.remote.api.ApiMapper;
import com.myst3ry.domain.model.request.AuthRequest;
import com.myst3ry.domain.repository.AuthRepository;

import io.reactivex.Completable;
import io.reactivex.Single;

public class AuthRepositoryImpl implements AuthRepository {

    private final ApiMapper apiMapper;

    public AuthRepositoryImpl(final ApiMapper apiMapper) {
        this.apiMapper = apiMapper;
    }

    @Override
    public Single<Boolean> signIn(final AuthRequest request) {
        return apiMapper.signIn(request)
                .flatMap((user) -> Single.just(!TextUtils.isEmpty(user.getFirstName())
                        && !TextUtils.isEmpty(user.getLastName()))
                );
    }

    @Override
    public Completable signOut() {
        return apiMapper.signOut();
    }
}
