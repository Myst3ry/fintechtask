package com.myst3ry.data.repository;

import com.myst3ry.data.mapper.CourseDataMapper;
import com.myst3ry.data.source.local.cache.Cache;
import com.myst3ry.data.source.remote.api.ApiMapper;
import com.myst3ry.domain.model.Course;
import com.myst3ry.domain.repository.CoursesRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class CoursesRepositoryImpl implements CoursesRepository {

    private final ApiMapper apiMapper;
    private final Cache cache;

    public CoursesRepositoryImpl(final ApiMapper apiMapper, final Cache cache) {
        this.apiMapper = apiMapper;
        this.cache = cache;
    }

    @Override
    public Observable<List<Course>> getAvailableCourses() {
        return apiMapper.getAvailableCourses()
                .observeOn(Schedulers.computation())
                .map(CourseDataMapper::transform)
                .toObservable();
    }
}
