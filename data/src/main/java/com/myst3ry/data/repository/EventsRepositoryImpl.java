package com.myst3ry.data.repository;

import com.myst3ry.data.mapper.EventDataMapper;
import com.myst3ry.data.source.local.cache.Cache;
import com.myst3ry.data.source.remote.api.ApiMapper;
import com.myst3ry.domain.model.Event;
import com.myst3ry.domain.repository.EventsRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class EventsRepositoryImpl implements EventsRepository {

    private final ApiMapper apiMapper;
    private final Cache cache;

    public EventsRepositoryImpl(final ApiMapper apiMapper, final Cache cache) {
        this.apiMapper = apiMapper;
        this.cache = cache;
    }

    @Override
    public Observable<List<Event>> getActiveEvents() {
        return apiMapper.getActiveEvents()
                .observeOn(Schedulers.computation())
                .map(EventDataMapper::transform)
                .toObservable();
    }

    @Override
    public Observable<List<Event>> getArchiveEvents() {
        return apiMapper.getArchiveEvents()
                .observeOn(Schedulers.computation())
                .map(EventDataMapper::transform)
                .toObservable();
    }
}
