package com.myst3ry.data.repository;

import com.myst3ry.data.mapper.GradeDataMapper;
import com.myst3ry.data.source.local.cache.Cache;
import com.myst3ry.data.source.remote.api.ApiMapper;
import com.myst3ry.domain.model.Grade;
import com.myst3ry.domain.repository.GradebookRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class GradebookRepositoryImpl implements GradebookRepository {

    private final ApiMapper apiMapper;
    private final Cache cache;

    public GradebookRepositoryImpl(final ApiMapper apiMapper, final Cache cache) {
        this.apiMapper = apiMapper;
        this.cache = cache;
    }

    public Observable<List<Grade>> getCourseGradebook(final String courseUrl) {
        return apiMapper.getCourseGradebook(courseUrl)
                .observeOn(Schedulers.computation())
                .map(GradeDataMapper::transform);
    }
}
