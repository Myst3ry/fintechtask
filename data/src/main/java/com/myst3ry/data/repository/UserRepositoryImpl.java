package com.myst3ry.data.repository;

import com.myst3ry.data.mapper.UserDataMapper;
import com.myst3ry.data.source.local.cache.Cache;
import com.myst3ry.data.source.remote.api.ApiMapper;
import com.myst3ry.domain.model.User;
import com.myst3ry.domain.repository.UserRepository;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class UserRepositoryImpl implements UserRepository {

    private final ApiMapper apiMapper;
    private final Cache cache;

    public UserRepositoryImpl(final ApiMapper apiMapper, final Cache cache) {
        this.apiMapper = apiMapper;
        this.cache = cache;
    }

    @Override
    public Observable<User> getUserProfile() {
        return apiMapper.getUserProfile()
                .observeOn(Schedulers.computation())
                .map(UserDataMapper::transform)
                .toObservable();
    }
}
