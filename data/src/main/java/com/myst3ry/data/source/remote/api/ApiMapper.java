package com.myst3ry.data.source.remote.api;

import com.myst3ry.data.source.remote.pojo.courses.CourseData;
import com.myst3ry.data.source.remote.pojo.events.EventData;
import com.myst3ry.data.source.remote.pojo.grades.GradeData;
import com.myst3ry.domain.model.request.AuthRequest;
import com.myst3ry.data.source.remote.pojo.auth.AuthResponse;
import com.myst3ry.data.source.remote.pojo.courses.CoursesResponse;
import com.myst3ry.data.source.remote.pojo.events.EventsResponse;
import com.myst3ry.data.source.remote.pojo.grades.GradebookResponse;
import com.myst3ry.data.source.remote.pojo.user.UserData;
import com.myst3ry.data.source.remote.pojo.user.UserResponse;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class ApiMapper {

    private final FintechApi mApiService;

    public ApiMapper(final FintechApi apiService) {
        this.mApiService = apiService;
    }

    //todo pair

    public Single<List<EventData>> getActiveEvents() {
        return mApiService.getEvents()
                .map(EventsResponse::getActiveEvents);
    }

    public Single<List<EventData>> getArchiveEvents() {
        return mApiService.getEvents()
                .map(EventsResponse::getArchiveEvents);
    }

    public Single<UserData> getUserProfile() {
        return mApiService.getUser()
                .map(UserResponse::getUser);
    }

    public Single<List<CourseData>> getAvailableCourses() {
        return mApiService.getCourses()
                .map(CoursesResponse::getCourses);
    }

    public Observable<List<GradeData>> getCourseGradebook(final String url) {
        return mApiService.getGradebook(url)
                .map(GradebookResponse::getGrades);
    }

    public Single<AuthResponse> signIn(final AuthRequest request) {
        return mApiService.signIn(request);
    }

    public Completable signOut() {
        return mApiService.signOut();
    }
}
