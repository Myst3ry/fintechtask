package com.myst3ry.data.source.remote.api;

import com.myst3ry.domain.model.request.AuthRequest;
import com.myst3ry.data.source.remote.pojo.auth.AuthResponse;
import com.myst3ry.data.source.remote.pojo.courses.CoursesResponse;
import com.myst3ry.data.source.remote.pojo.events.EventsResponse;
import com.myst3ry.data.source.remote.pojo.grades.GradebookResponse;
import com.myst3ry.data.source.remote.pojo.user.UserResponse;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface FintechApi {

    String BASE_URL = "https://fintech.tinkoff.ru";

    @GET("/api/calendar/list/event")
    Single<EventsResponse> getEvents();

    @POST("/api/signin")
    Single<AuthResponse> signIn(@Body final AuthRequest request);

    @POST("/api/signout")
    Completable signOut();

    @GET("/api/user")
    Single<UserResponse> getUser();

    @GET("/api/connections")
    Single<CoursesResponse> getCourses();

    @GET("/api/course/{url}/grades")
    Observable<GradebookResponse> getGradebook(@Path("url") final String courseUrl);
}
