package com.myst3ry.data.source.remote.pojo.courses;

import com.google.gson.annotations.SerializedName;

public class CourseData {

	@SerializedName("event_date_start")
	private String eventDateStart;

	@SerializedName("title")
	private String title;

	@SerializedName("url")
	private String url;

	@SerializedName("status")
	private String status;

	public String getEventDateStart(){
		return eventDateStart;
	}

	public String getTitle(){
		return title;
	}

	public String getUrl(){
		return url;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"CourseData{" +
			"event_date_start = '" + eventDateStart + '\'' + 
			",title = '" + title + '\'' + 
			",url = '" + url + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}