package com.myst3ry.data.source.remote.pojo.courses;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class CoursesResponse {

    @SerializedName("courses")
    private List<CourseData> courses;

    public List<CourseData> getCourses() {
        return courses;
    }
}