package com.myst3ry.data.source.remote.pojo.events;

import com.google.gson.annotations.SerializedName;

public class EventData {

    @SerializedName("url_text")
    private String urlText;

    @SerializedName("date_start")
    private String dateStart;

    @SerializedName("event_type")
    private EventType eventType;

    @SerializedName("custom_date")
    private String customDate;

    @SerializedName("url_external")
    private boolean urlExternal;

    @SerializedName("display_button")
    private boolean displayButton;

    @SerializedName("description")
    private String description;

    @SerializedName("date_end")
    private String dateEnd;

    @SerializedName("place")
    private String place;

    @SerializedName("title")
    private String title;

    @SerializedName("url")
    private String url;

    public String getUrlText() {
        return urlText;
    }

    public String getDateStart() {
        return dateStart;
    }

    public EventType getEventType() {
        return eventType;
    }

    public String getCustomDate() {
        return customDate;
    }

    public boolean isUrlExternal() {
        return urlExternal;
    }

    public boolean isDisplayButton() {
        return displayButton;
    }

    public String getDescription() {
        return description;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public String getPlace() {
        return place;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return "EventData{" +
                "url_text = '" + urlText + '\'' +
                ",date_start = '" + dateStart + '\'' +
                ",event_type = '" + eventType + '\'' +
                ",custom_date = '" + customDate + '\'' +
                ",url_external = '" + urlExternal + '\'' +
                ",display_button = '" + displayButton + '\'' +
                ",description = '" + description + '\'' +
                ",date_end = '" + dateEnd + '\'' +
                ",place = '" + place + '\'' +
                ",title = '" + title + '\'' +
                ",url = '" + url + '\'' +
                "}";
    }
}