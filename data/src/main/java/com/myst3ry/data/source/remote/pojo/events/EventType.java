package com.myst3ry.data.source.remote.pojo.events;

import com.google.gson.annotations.SerializedName;

public class EventType {

    @SerializedName("color")
    private String color;

    @SerializedName("name")
    private String name;

    public String getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "EventType{" +
                "color = '" + color + '\'' +
                ",name = '" + name + '\'' +
                "}";
    }
}