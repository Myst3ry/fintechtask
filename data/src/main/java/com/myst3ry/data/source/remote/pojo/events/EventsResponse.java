package com.myst3ry.data.source.remote.pojo.events;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class EventsResponse {

    @SerializedName("active")
    private List<EventData> activeEvents;

    @SerializedName("archive")
    private List<EventData> archiveEvents;

    public List<EventData> getActiveEvents() {
        return activeEvents;
    }

    public List<EventData> getArchiveEvents() {
        return archiveEvents;
    }
}