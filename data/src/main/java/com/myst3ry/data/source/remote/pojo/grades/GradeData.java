package com.myst3ry.data.source.remote.pojo.grades;

import com.google.gson.annotations.SerializedName;

public class GradeData {

    @SerializedName("id")
    private int id;

    @SerializedName("task_type")
    private String taskType;

    @SerializedName("mark")
    private String mark;

    @SerializedName("status")
    private String status;

    public int getId() {
        return id;
    }

    public String getTaskType() {
        return taskType;
    }

    public String getMark() {
        return mark;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "GradeData{" +
                "id = '" + id + '\'' +
                ",task_type = '" + taskType + '\'' +
                ",mark = '" + mark + '\'' +
                ",status = '" + status + '\'' +
                "}";
    }
}