package com.myst3ry.data.source.remote.pojo.grades;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class GradebookResponse {

    @SerializedName("mentor")
    private String mentor;

    @SerializedName("mentor_id")
    private int mentorId;

    @SerializedName("name")
    private String name;

    @SerializedName("grades")
    private List<GradeData> grades;

    @SerializedName("id")
    private int id;

    @SerializedName("grouped_tasks")
    private List<GroupedTask> groupedTasks;

    public Object getMentor() {
        return mentor;
    }

    public Object getMentorId() {
        return mentorId;
    }

    public String getName() {
        return name;
    }

    public List<GradeData> getGrades() {
        return grades;
    }

    public int getId() {
        return id;
    }

    public List<GroupedTask> getGroupedTasks() {
        return groupedTasks;
    }

    @Override
    public String toString() {
        return "GradebookResponse{" +
                "mentor = '" + mentor + '\'' +
                ",mentor_id = '" + mentorId + '\'' +
                ",name = '" + name + '\'' +
                ",grades = '" + grades + '\'' +
                ",id = '" + id + '\'' +
                ",grouped_tasks = '" + groupedTasks + '\'' +
                "}";
    }
}