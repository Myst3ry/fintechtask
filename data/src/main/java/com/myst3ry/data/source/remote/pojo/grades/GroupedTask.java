package com.myst3ry.data.source.remote.pojo.grades;

import com.google.gson.annotations.SerializedName;

public class GroupedTask {

    @SerializedName("max_score")
    private int maxScore;

    @SerializedName("short_name")
    private String shortName;

    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;

    public int getMaxScore() {
        return maxScore;
    }

    public String getShortName() {
        return shortName;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "GroupedTask {" +
                "max_score = '" + maxScore + '\'' +
                ",short_name = '" + shortName + '\'' +
                ",id = '" + id + '\'' +
                ",title = '" + title + '\'' +
                "}";
    }
}