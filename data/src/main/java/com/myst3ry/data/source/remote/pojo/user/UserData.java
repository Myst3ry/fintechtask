package com.myst3ry.data.source.remote.pojo.user;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class UserData {

    @SerializedName("birthday")
    private String birthday;

    @SerializedName("resume")
    private String resume;

    @SerializedName("phone_mobile")
    private String phoneMobile;

    @SerializedName("skype_login")
    private String skypeLogin;

    @SerializedName("university")
    private String university;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("description")
    private String description;

    @SerializedName("admin")
    private boolean admin;

    @SerializedName("avatar")
    private String avatar;

    @SerializedName("middle_name")
    private String middleName;

    @SerializedName("faculty")
    private String faculty;

    @SerializedName("school")
    private String school;

    @SerializedName("grade")
    private String grade;

    @SerializedName("current_work")
    private String currentWork;

    @SerializedName("t_shirt_size")
    private String tShirtSize;

    @SerializedName("school_graduation")
    private int schoolGraduation;

    @SerializedName("is_client")
    private boolean isClient;

    @SerializedName("university_graduation")
    private int universityGraduation;

    @SerializedName("id")
    private int id;

    @SerializedName("region")
    private String region;

    @SerializedName("department")
    private String department;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("email")
    private String email;

    @SerializedName("notifications")
    private List<String> notifications;

    public String getBirthday() {
        return birthday;
    }

    public String getResume() {
        return resume;
    }

    public String getPhoneMobile() {
        return phoneMobile;
    }

    public String getSkypeLogin() {
        return skypeLogin;
    }

    public String getUniversity() {
        return university;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDescription() {
        return description;
    }

    public boolean isAdmin() {
        return admin;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getSchool() {
        return school;
    }

    public String getGrade() {
        return grade;
    }

    public String getCurrentWork() {
        return currentWork;
    }

    public String getTShirtSize() {
        return tShirtSize;
    }

    public int getSchoolGraduation() {
        return schoolGraduation;
    }

    public boolean isIsClient() {
        return isClient;
    }

    public int getUniversityGraduation() {
        return universityGraduation;
    }

    public int getId() {
        return id;
    }

    public String getRegion() {
        return region;
    }

    public String getDepartment() {
        return department;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getEmail() {
        return email;
    }

    public List<String> getNotifications() {
        return notifications;
    }

    @Override
    public String toString() {
        return "UserData {" +
                        "birthday = '" + birthday + '\'' +
                        ",resume = '" + resume + '\'' +
                        ",phone_mobile = '" + phoneMobile + '\'' +
                        ",skype_login = '" + skypeLogin + '\'' +
                        ",university = '" + university + '\'' +
                        ",last_name = '" + lastName + '\'' +
                        ",description = '" + description + '\'' +
                        ",admin = '" + admin + '\'' +
                        ",avatar = '" + avatar + '\'' +
                        ",middle_name = '" + middleName + '\'' +
                        ",faculty = '" + faculty + '\'' +
                        ",school = '" + school + '\'' +
                        ",grade = '" + grade + '\'' +
                        ",current_work = '" + currentWork + '\'' +
                        ",t_shirt_size = '" + tShirtSize + '\'' +
                        ",school_graduation = '" + schoolGraduation + '\'' +
                        ",is_client = '" + isClient + '\'' +
                        ",university_graduation = '" + universityGraduation + '\'' +
                        ",id = '" + id + '\'' +
                        ",region = '" + region + '\'' +
                        ",department = '" + department + '\'' +
                        ",first_name = '" + firstName + '\'' +
                        ",email = '" + email + '\'' +
                        ",notifications = '" + notifications + '\'' +
                        "}";
    }
}