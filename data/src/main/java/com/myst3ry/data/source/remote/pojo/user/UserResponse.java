package com.myst3ry.data.source.remote.pojo.user;

import com.google.gson.annotations.SerializedName;

public class UserResponse {

    @SerializedName("user")
    private UserData user;

    @SerializedName("status")
    private String status;

    public UserData getUser() {
        return user;
    }

    public String getStatus() {
        return status;
    }
}