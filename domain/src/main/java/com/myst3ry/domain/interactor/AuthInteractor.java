package com.myst3ry.domain.interactor;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface AuthInteractor {

    Single<Boolean> signIn(final String email, final String password);

    Completable signOut();
}
