package com.myst3ry.domain.interactor;

import com.myst3ry.domain.model.request.AuthRequest;
import com.myst3ry.domain.repository.AuthRepository;

import io.reactivex.Completable;
import io.reactivex.Single;

public class AuthInteractorImpl implements AuthInteractor {

    private final AuthRepository authRepository;

    public AuthInteractorImpl(final AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    @Override
    public Single<Boolean> signIn(final String email, final String password) {
        return authRepository.signIn(new AuthRequest(email, password));
    }

    @Override
    public Completable signOut() {
        return authRepository.signOut();
    }
}
