package com.myst3ry.domain.interactor;

import com.myst3ry.domain.model.Course;

import java.util.List;

import io.reactivex.Observable;

public interface CoursesInteractor {

    Observable<List<Course>> getAvailableCourses();
}
