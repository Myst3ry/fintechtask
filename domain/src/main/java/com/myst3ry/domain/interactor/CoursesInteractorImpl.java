package com.myst3ry.domain.interactor;

import com.myst3ry.domain.model.Course;
import com.myst3ry.domain.repository.CoursesRepository;

import java.util.List;

import io.reactivex.Observable;

public class CoursesInteractorImpl implements CoursesInteractor {

    private final CoursesRepository coursesRepository;

    public CoursesInteractorImpl(final CoursesRepository coursesRepository) {
        this.coursesRepository = coursesRepository;
    }

    @Override
    public Observable<List<Course>> getAvailableCourses() {
        return coursesRepository.getAvailableCourses();
    }
}
