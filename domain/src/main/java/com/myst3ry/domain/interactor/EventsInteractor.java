package com.myst3ry.domain.interactor;

import com.myst3ry.domain.model.Event;

import java.util.List;

import io.reactivex.Observable;

public interface EventsInteractor {

    Observable<List<Event>> getActiveEvents();

    Observable<List<Event>> getArchiveEvents();
}
