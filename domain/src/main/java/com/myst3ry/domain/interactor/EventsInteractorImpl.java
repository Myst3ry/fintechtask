package com.myst3ry.domain.interactor;

import com.myst3ry.domain.model.Event;
import com.myst3ry.domain.repository.EventsRepository;

import java.util.List;

import io.reactivex.Observable;

public class EventsInteractorImpl implements EventsInteractor {

    private final EventsRepository eventsRepository;

    public EventsInteractorImpl(final EventsRepository eventsRepository) {
        this.eventsRepository = eventsRepository;
    }

    @Override
    public Observable<List<Event>> getActiveEvents() {
        return eventsRepository.getActiveEvents();
    }

    @Override
    public Observable<List<Event>> getArchiveEvents() {
        return eventsRepository.getArchiveEvents();
    }
}
