package com.myst3ry.domain.interactor;

import com.myst3ry.domain.model.Grade;

import java.util.List;

import io.reactivex.Observable;

public interface GradebookInteractor {

    Observable<List<Grade>> getCourseGradebook(final String courseUrl);
}
