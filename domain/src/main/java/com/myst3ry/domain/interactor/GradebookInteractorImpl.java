package com.myst3ry.domain.interactor;

import com.myst3ry.domain.model.Grade;
import com.myst3ry.domain.repository.GradebookRepository;

import java.util.List;

import io.reactivex.Observable;

public class GradebookInteractorImpl implements GradebookInteractor {

    private final GradebookRepository gradebookRepository;

    public GradebookInteractorImpl(final GradebookRepository gradebookRepository) {
        this.gradebookRepository = gradebookRepository;
    }

    @Override
    public Observable<List<Grade>> getCourseGradebook(final String courseUrl) {
        return gradebookRepository.getCourseGradebook(courseUrl);
    }
}
