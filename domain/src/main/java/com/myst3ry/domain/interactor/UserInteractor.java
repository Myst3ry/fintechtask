package com.myst3ry.domain.interactor;

import com.myst3ry.domain.model.User;

import io.reactivex.Completable;
import io.reactivex.Observable;

public interface UserInteractor {

    Observable<User> getUserProfile();
}
