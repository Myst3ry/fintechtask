package com.myst3ry.domain.interactor;

import com.myst3ry.domain.model.User;
import com.myst3ry.domain.repository.UserRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;

public class UserInteractorImpl implements UserInteractor {

    private final UserRepository userRepository;

    public UserInteractorImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Observable<User> getUserProfile() {
        return userRepository.getUserProfile();
    }
}
