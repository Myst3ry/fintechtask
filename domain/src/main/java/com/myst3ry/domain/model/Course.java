package com.myst3ry.domain.model;

public class Course {

    private String eventDateStart;
    private String title;
    private String url;
    private String status;

    public Course(String eventDateStart, String title, String url, String status) {
        this.eventDateStart = eventDateStart;
        this.title = title;
        this.url = url;
        this.status = status;
    }

    public String getEventDateStart() {
        return eventDateStart;
    }

    public void setEventDateStart(String eventDateStart) {
        this.eventDateStart = eventDateStart;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Course{" +
                "eventDateStart='" + eventDateStart + '\'' +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
