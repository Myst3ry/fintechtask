package com.myst3ry.domain.model.request;

public class AuthRequest {

    //{"email":" user@gmail.com ","password":"1234test"}

    private final String email;
    private final String password;

    public AuthRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }
}