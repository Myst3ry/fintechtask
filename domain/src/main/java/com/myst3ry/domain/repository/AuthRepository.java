package com.myst3ry.domain.repository;

import com.myst3ry.domain.model.request.AuthRequest;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface AuthRepository {

    Single<Boolean> signIn(final AuthRequest request);

    Completable signOut();
}
