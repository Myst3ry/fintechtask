package com.myst3ry.domain.repository;

import com.myst3ry.domain.model.Course;

import java.util.List;

import io.reactivex.Observable;

public interface CoursesRepository {

    Observable<List<Course>> getAvailableCourses();
}
