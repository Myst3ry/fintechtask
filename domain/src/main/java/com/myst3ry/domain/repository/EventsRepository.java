package com.myst3ry.domain.repository;

import com.myst3ry.domain.model.Event;

import java.util.List;

import io.reactivex.Observable;

public interface EventsRepository {

    Observable<List<Event>> getActiveEvents();

    Observable<List<Event>> getArchiveEvents();
}
