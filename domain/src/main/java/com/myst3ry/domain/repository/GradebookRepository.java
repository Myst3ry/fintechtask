package com.myst3ry.domain.repository;

import com.myst3ry.domain.model.Grade;

import java.util.List;

import io.reactivex.Observable;

public interface GradebookRepository {

    Observable<List<Grade>> getCourseGradebook(final String courseUrl);
}
