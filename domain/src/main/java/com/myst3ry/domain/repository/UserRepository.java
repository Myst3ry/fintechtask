package com.myst3ry.domain.repository;

import com.myst3ry.domain.model.User;

import io.reactivex.Observable;

public interface UserRepository {

    Observable<User> getUserProfile();
}
