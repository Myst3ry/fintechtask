package com.myst3ry.domain.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class ConvertUtils {

    private static final SimpleDateFormat inputDateFormat =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
    private static final SimpleDateFormat outputDateFormat  =
            new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());

    private ConvertUtils() { }

    public static String parseDate(final String date) {
        try {
            return outputDateFormat.format(inputDateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }
    }

    public static String formatEventDate(final String rawDateStart, final String rawDateEnd) {
        return String.format(Locale.getDefault(), "%s - %s",
               parseDate(rawDateStart), parseDate(rawDateEnd));
    }
}
