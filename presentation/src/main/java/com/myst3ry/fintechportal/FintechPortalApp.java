package com.myst3ry.fintechportal;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.myst3ry.fintechportal.di.component.AppComponent;
import com.myst3ry.fintechportal.di.component.DaggerAppComponent;
import com.myst3ry.fintechportal.di.module.AppModule;

import timber.log.Timber;

public class FintechPortalApp extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initAppComponent();
        configureTimber();
    }

    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    private void configureTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree() {
                @Override
                protected String createStackElementTag(@NonNull StackTraceElement element) {
                    return super.createStackElementTag(element) + ":" + element.getLineNumber();
                }
            });
        } else {
            Timber.plant(new TimberReleaseTree());
        }
    }

    public static FintechPortalApp get(final Context context) {
        return ((FintechPortalApp) context.getApplicationContext());
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
