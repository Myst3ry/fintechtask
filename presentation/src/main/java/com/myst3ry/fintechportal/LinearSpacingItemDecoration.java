package com.myst3ry.fintechportal;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.view.View;

public class LinearSpacingItemDecoration extends ItemDecoration {

    private static final int ZERO = 0;
    private static final int HALF_DIVIDER = 2;
    private static final int LAST_CELL_OFFSET = 1;

    private final int spacing;
    private final boolean includeEdge;
    private final boolean isVertical;

    private LinearSpacingItemDecoration(final Builder builder) {
        spacing = builder.spacing;
        includeEdge = builder.includeEdge;
        isVertical = builder.orientation == LinearLayoutManager.VERTICAL;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public void getItemOffsets(@NonNull final Rect outRect,
                               @NonNull final View view,
                               @NonNull final RecyclerView parent,
                               @NonNull final RecyclerView.State state) {
        final int position = parent.getChildAdapterPosition(view);
        final boolean isFirstCell = position == ZERO;
        final boolean isLastCell = position == parent.getAdapter().getItemCount() - LAST_CELL_OFFSET;

        if (isVertical) {
            outRect.left = spacing;
            outRect.right = spacing;
        }

        if (isFirstCell) {
            if (isVertical) {
                outRect.top = includeEdge ? spacing : ZERO;
                outRect.bottom = isLastCell ? (includeEdge ? spacing : ZERO) : ZERO;
            } else {
                outRect.left = includeEdge ? spacing : ZERO;
                outRect.right = spacing / HALF_DIVIDER;
            }
        } else if (isLastCell) {
            if (isVertical) {
                outRect.top = ZERO;
                outRect.bottom = includeEdge ? spacing : ZERO;
            } else {
                outRect.left = spacing / HALF_DIVIDER;
                outRect.right = includeEdge ? spacing : ZERO;
            }
        } else {
            if (isVertical) {
                outRect.top = spacing;
                outRect.bottom = spacing;
            } else {
                outRect.left = spacing / HALF_DIVIDER;
                outRect.right = spacing / HALF_DIVIDER;
            }
        }
    }

    public static final class Builder {
        private int spacing;
        private boolean includeEdge;
        private int orientation;

        public Builder setSpacing(final int spacing) {
            this.spacing = spacing;
            return this;
        }

        public Builder includeEdge(final boolean includeEdge) {
            this.includeEdge = includeEdge;
            return this;
        }

        public Builder setOrientation(final int orientation) {
            this.orientation = orientation;
            return this;
        }

        public LinearSpacingItemDecoration build() {
            return new LinearSpacingItemDecoration(this);
        }

        private Builder() {
        }
    }

}
