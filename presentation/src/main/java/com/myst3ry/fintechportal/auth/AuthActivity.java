package com.myst3ry.fintechportal.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.myst3ry.fintechportal.FintechPortalApp;
import com.myst3ry.fintechportal.R;
import com.myst3ry.fintechportal.base.BaseActivity;
import com.myst3ry.fintechportal.courses.CoursesActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class AuthActivity extends BaseActivity implements AuthView {

    @BindView(R.id.logo)
    ImageView logoImageView;
    @BindView(R.id.et_login)
    TextInputEditText loginEditText;
    @BindView(R.id.et_password)
    TextInputEditText passwordEditText;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @Inject
    AuthPresenter authPresenter;

    public static Intent newIntent(Context context) {
        return new Intent(context, AuthActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        FintechPortalApp.get(this).getAppComponent().getAuthSubComponent().inject(this);
        authPresenter.attachView(this);
        setFocusListeners();
    }

    @Override
    public void proceedSignIn() {
        this.startActivity(CoursesActivity.newIntent(this));
    }

    @Override
    public void proceedSignOut() {
        this.finish();
    }

    @Override
    public void showProgressBar() {
        if (progressBar != null && progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgressBar() {
        if (progressBar != null && progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError(String errorText) {
        Toast.makeText(this, errorText, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        logoImageView.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_signin)
    void onSignInButtonClick() {
        performSignIn();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(isDestroyed()) {
            authPresenter.detachView();
            authPresenter.disposeAll();
        }
    }

    private void setFocusListeners() {
        loginEditText.setOnFocusChangeListener(new OnFocusChangeListener());
        passwordEditText.setOnFocusChangeListener(new OnFocusChangeListener());
    }

    private void performSignIn() {
        Timber.w("Sign in");

        if (TextUtils.isEmpty(loginEditText.getText())) {
            loginEditText.setError(getString(R.string.empty_field_err));
            return;
        }

        if (TextUtils.isEmpty(passwordEditText.getText())) {
            passwordEditText.setError(getString(R.string.empty_field_err));
            return;
        }

        authPresenter.signIn(loginEditText.getText().toString(),
                passwordEditText.getText().toString());
    }

    private class OnFocusChangeListener implements View.OnFocusChangeListener {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            logoImageView.setVisibility(View.GONE);
        }
    }
}
