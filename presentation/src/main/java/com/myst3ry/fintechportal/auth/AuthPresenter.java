package com.myst3ry.fintechportal.auth;

import com.myst3ry.domain.interactor.AuthInteractor;
import com.myst3ry.fintechportal.base.BasePresenter;
import com.myst3ry.fintechportal.di.scope.AuthScope;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@AuthScope
public class AuthPresenter extends BasePresenter<AuthView> {

    private final AuthInteractor authInteractor;

    public AuthPresenter(final AuthInteractor authInteractor) {
        this.authInteractor = authInteractor;
    }

    public void signIn(final String email, final String password) {
        getView().showProgressBar();
        addDisposable(authInteractor.signIn(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSignInSuccess, this::onError)
        );
    }

    public void signOut() {
        getView().showProgressBar();
        addDisposable(authInteractor.signOut()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSignOutSuccess, this::onError)
        );
    }

    private void onSignInSuccess(final boolean isSuccess) {
        if (isSuccess) {
            getView().hideProgressBar();
            getView().proceedSignIn();
        }
    }

    private void onSignOutSuccess() {
        getView().hideProgressBar();
        getView().proceedSignOut();
    }

    private void onError(final Throwable throwable) {
        getView().hideProgressBar();
        getView().showError(throwable.getLocalizedMessage());
    }
}
