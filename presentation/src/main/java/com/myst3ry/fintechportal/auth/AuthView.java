package com.myst3ry.fintechportal.auth;

import com.myst3ry.fintechportal.base.BaseView;

public interface AuthView extends BaseView {

    void proceedSignIn();

    void proceedSignOut();

    void showProgressBar();

    void hideProgressBar();

    void showError(final String errorText);
}
