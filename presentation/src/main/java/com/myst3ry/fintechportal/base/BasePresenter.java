package com.myst3ry.fintechportal.base;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BasePresenter<T extends BaseView> {

    private T view;
    private CompositeDisposable disposables = new CompositeDisposable();

    public void attachView(T view) {
        this.view = view;
    }

    protected void addDisposable(final Disposable disposable) {
        if (disposable != null) {
            disposables.add(disposable);
        }
    }

    public void disposeAll() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    public T getView() {
        return view;
    }

    public void detachView() {
        this.view = null;
    }
}
