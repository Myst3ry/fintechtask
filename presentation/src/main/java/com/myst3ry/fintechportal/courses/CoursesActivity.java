package com.myst3ry.fintechportal.courses;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;

import com.myst3ry.fintechportal.R;
import com.myst3ry.fintechportal.base.BaseActivity;
import com.myst3ry.fintechportal.courses.gradebook.GradebookFragment;

public class CoursesActivity extends BaseActivity implements OnCourseClickListener {

    public static Intent newIntent(Context context) {
        return new Intent(context, CoursesActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courses);
        if (savedInstanceState == null) {
            initUI();
        }
        prepareActionBar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCourseClick(String courseUrl) {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_courses, GradebookFragment.newInstance(courseUrl), GradebookFragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void initUI() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_courses, CoursesFragment.newInstance(), CoursesFragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void prepareActionBar() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
