package com.myst3ry.fintechportal.courses;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myst3ry.domain.model.Course;
import com.myst3ry.fintechportal.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CoursesAdapter extends RecyclerView.Adapter<CoursesAdapter.CourseHolder> {

    private List<Course> courses;
    private final OnCourseClickListener listener;

    public CoursesAdapter(final OnCourseClickListener listener) {
        this.listener = listener;
        this.courses = new ArrayList<>();
    }

    @NonNull
    @Override
    public CourseHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        return new CourseHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_course, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final CourseHolder holder, final int position) {
        holder.bind(getCourse(position));
    }

    @Override
    public int getItemCount() {
        return courses.size();
    }

    public void setCourses(final List<Course> courses) {
        this.courses = courses;
        notifyDataSetChanged();
    }

    private Course getCourse(final int position) {
        return courses.get(position);
    }

    final class CourseHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_course_title)
        TextView courseTitleTextView;
        @BindView(R.id.tv_course_status)
        TextView courseStatusTextView;
        @BindView(R.id.tv_course_date_start)
        TextView courseDateStartTextView;

        @OnClick(R.id.course_container)
        void onCourseClick() {
            listener.onCourseClick(getCourse(getLayoutPosition()).getUrl());
        }

        CourseHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(final Course currentCourse) {
            courseTitleTextView.setText(currentCourse.getTitle());
            courseStatusTextView.setText(currentCourse.getStatus());
            courseDateStartTextView.setText(currentCourse.getEventDateStart());
        }
    }
}
