package com.myst3ry.fintechportal.courses;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.myst3ry.domain.model.Course;
import com.myst3ry.fintechportal.FintechPortalApp;
import com.myst3ry.fintechportal.LinearSpacingItemDecoration;
import com.myst3ry.fintechportal.R;
import com.myst3ry.fintechportal.base.BaseFragment;
import com.myst3ry.fintechportal.user.UserProfileActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class CoursesFragment extends BaseFragment implements CoursesView {

    public static final String TAG = CoursesFragment.class.getSimpleName();

    @BindView(R.id.rv_courses)
    RecyclerView coursesRecyclerView;
    @BindView(R.id.tv_empty_courses)
    TextView emptyTextView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @Inject
    CoursesPresenter coursesPresenter;

    private OnCourseClickListener listener;
    private CoursesAdapter adapter;

    public static CoursesFragment newInstance() {
        return new CoursesFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCourseClickListener) {
            this.listener = (OnCourseClickListener) context;
        } else {
            throw new ClassCastException("Activity must implement OnCourseClickListener interface");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FintechPortalApp.get(getActivity()).getAppComponent().getCoursesSubComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_courses, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        coursesPresenter.attachView(this);
        initAdapter();
        prepareRecyclerView();
    }

    @Override
    public void onStart() {
        super.onStart();
        getAvailableCourses();
    }

    @Override
    public void setCoursesInfo(final List<Course> courses) {
        adapter.setCourses(courses);
    }

    @Override
    public void showProgressBar() {
        if (progressBar != null && progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgressBar() {
        if (progressBar != null && progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showEmptyText() {
        if (emptyTextView != null && emptyTextView.getVisibility() == View.GONE) {
            coursesRecyclerView.setVisibility(View.GONE);
            emptyTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showError(final String errorText) {
        Toast.makeText(getActivity(), errorText, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideEmptyText() {
        if (emptyTextView != null && emptyTextView.getVisibility() == View.VISIBLE) {
            emptyTextView.setVisibility(View.GONE);
            coursesRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        coursesPresenter.detachView();
        coursesPresenter.disposeAll();
    }

    @OnClick(R.id.tv_courses_profile)
    void onProfileClick() {
        startActivity(UserProfileActivity.newIntent(getActivity()));
    }

    private void getAvailableCourses() {
        coursesPresenter.getAvailableCourses();
    }

    private void initAdapter() {
        adapter = new CoursesAdapter(listener);
    }

    private void prepareRecyclerView() {
        coursesRecyclerView.setAdapter(adapter);
        coursesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false));
        coursesRecyclerView.addItemDecoration(LinearSpacingItemDecoration.newBuilder()
                .setSpacing(getResources().getDimensionPixelSize(R.dimen.margin_half))
                .setOrientation(LinearLayoutManager.VERTICAL)
                .includeEdge(true)
                .build());
    }
}
