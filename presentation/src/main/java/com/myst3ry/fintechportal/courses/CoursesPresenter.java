package com.myst3ry.fintechportal.courses;

import com.myst3ry.domain.interactor.CoursesInteractor;
import com.myst3ry.domain.model.Course;
import com.myst3ry.fintechportal.base.BasePresenter;
import com.myst3ry.fintechportal.di.scope.CoursesScope;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@CoursesScope
public class CoursesPresenter extends BasePresenter<CoursesView> {

    private final CoursesInteractor coursesInteractor;

    public CoursesPresenter(final CoursesInteractor coursesInteractor) {
        this.coursesInteractor = coursesInteractor;
    }

    public void getAvailableCourses() {
        getView().showProgressBar();
        addDisposable(coursesInteractor.getAvailableCourses()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onError)
        );
    }

    private void onSuccess(final List<Course> courses) {
        getView().hideProgressBar();
        if (courses != null && !courses.isEmpty()) {
            getView().hideEmptyText();
            getView().setCoursesInfo(courses);
        } else {
            getView().showEmptyText();
        }
    }

    private void onError(final Throwable throwable) {
        getView().hideProgressBar();
        getView().showError(throwable.getLocalizedMessage());
    }
}
