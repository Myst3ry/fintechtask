package com.myst3ry.fintechportal.courses;

import com.myst3ry.domain.model.Course;
import com.myst3ry.fintechportal.base.BaseView;

import java.util.List;

public interface CoursesView extends BaseView {

    void setCoursesInfo(final List<Course> courses);

    void showProgressBar();

    void hideProgressBar();

    void showEmptyText();

    void hideEmptyText();

    void showError(final String errorText);
}
