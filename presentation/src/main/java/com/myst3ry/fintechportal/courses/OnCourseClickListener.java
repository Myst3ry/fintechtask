package com.myst3ry.fintechportal.courses;

public interface OnCourseClickListener {

    void onCourseClick(final String courseUrl);
}
