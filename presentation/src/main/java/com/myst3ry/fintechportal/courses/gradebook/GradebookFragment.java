package com.myst3ry.fintechportal.courses.gradebook;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.myst3ry.domain.model.Grade;
import com.myst3ry.fintechportal.BuildConfig;
import com.myst3ry.fintechportal.FintechPortalApp;
import com.myst3ry.fintechportal.R;
import com.myst3ry.fintechportal.base.BaseFragment;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;

public class GradebookFragment extends BaseFragment implements GradebookView {

    public static final String TAG = GradebookFragment.class.getSimpleName();
    private static final String ARG_COURSE_URL = BuildConfig.APPLICATION_ID + "ARG.COURSE_URL";

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @Inject
    GradebookPresenter gradebookPresenter;

    private String courseUrl;

    public static GradebookFragment newInstance(final String url) {
        final GradebookFragment fragment = new GradebookFragment();
        final Bundle args = new Bundle();
        args.putString(ARG_COURSE_URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FintechPortalApp.get(getActivity()).getAppComponent().getCoursesSubComponent().inject(this);
        courseUrl = Objects.requireNonNull(getArguments()).getString(ARG_COURSE_URL);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gradebook, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        gradebookPresenter.attachView(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        getCourseGradebook();
    }

    private void getCourseGradebook() {
        gradebookPresenter.getCourseGradebook(courseUrl);
    }

    @Override
    public void setGradebookInfo(final List<Grade> grades) {
        //todo impl
    }

    @Override
    public void showProgressBar() {
        if (progressBar != null && progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgressBar() {
        if (progressBar != null && progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError(final String errorText) {
        Toast.makeText(getActivity(), errorText, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        gradebookPresenter.detachView();
        gradebookPresenter.disposeAll();
    }
}
