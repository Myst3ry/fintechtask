package com.myst3ry.fintechportal.courses.gradebook;

import com.myst3ry.domain.interactor.GradebookInteractor;
import com.myst3ry.domain.model.Grade;
import com.myst3ry.fintechportal.base.BasePresenter;
import com.myst3ry.fintechportal.di.scope.CoursesScope;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@CoursesScope
public class GradebookPresenter extends BasePresenter<GradebookView> {

    private final GradebookInteractor gradebookInteractor;

    public GradebookPresenter(final GradebookInteractor gradebookInteractor) {
        this.gradebookInteractor = gradebookInteractor;
    }

    public void getCourseGradebook(final String courseUrl) {
        getView().showProgressBar();
        addDisposable(gradebookInteractor.getCourseGradebook(courseUrl)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onError)
        );
    }

    private void onSuccess(List<Grade> grades) {
        getView().hideProgressBar();
        getView().setGradebookInfo(grades);
    }

    private void onError(final Throwable throwable) {
        getView().hideProgressBar();
        getView().showError(throwable.getLocalizedMessage());
    }
}
