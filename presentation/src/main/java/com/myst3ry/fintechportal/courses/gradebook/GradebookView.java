package com.myst3ry.fintechportal.courses.gradebook;

import com.myst3ry.domain.model.Grade;
import com.myst3ry.fintechportal.base.BaseView;

import java.util.List;

public interface GradebookView extends BaseView {

    void setGradebookInfo(final List<Grade> grades);

    void showProgressBar();

    void hideProgressBar();

    void showError(final String errorText);
}
