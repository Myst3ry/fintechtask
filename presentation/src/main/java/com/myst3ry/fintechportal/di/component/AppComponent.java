package com.myst3ry.fintechportal.di.component;

import com.myst3ry.fintechportal.FPGlideModule;
import com.myst3ry.fintechportal.di.module.AppModule;
import com.myst3ry.fintechportal.di.module.DataModule;
import com.myst3ry.fintechportal.di.module.NetworkModule;
import com.myst3ry.fintechportal.events.EventsActivity;
import com.myst3ry.fintechportal.user.UserProfileActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, DataModule.class})
public interface AppComponent {

    void inject(final FPGlideModule glideModule);

    EventsSubComponent getEventsSubComponent();

    AuthSubComponent getAuthSubComponent();

    UserSubComponent getUserSubComponent();

    CoursesSubComponent getCoursesSubComponent();
}
