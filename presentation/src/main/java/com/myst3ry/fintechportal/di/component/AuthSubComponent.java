package com.myst3ry.fintechportal.di.component;

import com.myst3ry.fintechportal.auth.AuthActivity;
import com.myst3ry.fintechportal.di.module.AuthModule;
import com.myst3ry.fintechportal.di.scope.AuthScope;

import dagger.Subcomponent;

@AuthScope
@Subcomponent(modules = {AuthModule.class})
public interface AuthSubComponent {

    void inject(AuthActivity authActivity);
}
