package com.myst3ry.fintechportal.di.component;

import com.myst3ry.fintechportal.courses.CoursesFragment;
import com.myst3ry.fintechportal.di.module.CoursesModule;
import com.myst3ry.fintechportal.di.module.GradebookModule;
import com.myst3ry.fintechportal.di.scope.CoursesScope;
import com.myst3ry.fintechportal.courses.gradebook.GradebookFragment;

import dagger.Subcomponent;

@CoursesScope
@Subcomponent(modules = {CoursesModule.class, GradebookModule.class})
public interface CoursesSubComponent {

    void inject(CoursesFragment coursesFragment);

    void inject(GradebookFragment gradebookFragment);
}
