package com.myst3ry.fintechportal.di.component;

import com.myst3ry.fintechportal.di.module.EventsModule;
import com.myst3ry.fintechportal.di.scope.EventsScope;
import com.myst3ry.fintechportal.events.EventsActivity;

import dagger.Subcomponent;

@EventsScope
@Subcomponent(modules = {EventsModule.class})
public interface EventsSubComponent {

    void inject(EventsActivity eventsActivity);
}
