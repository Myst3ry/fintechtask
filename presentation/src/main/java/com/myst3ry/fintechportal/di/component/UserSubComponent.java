package com.myst3ry.fintechportal.di.component;

import com.myst3ry.fintechportal.di.module.AuthModule;
import com.myst3ry.fintechportal.di.module.UserModule;
import com.myst3ry.fintechportal.di.scope.UserScope;
import com.myst3ry.fintechportal.user.UserProfileActivity;

import dagger.Subcomponent;

@UserScope
@Subcomponent(modules = {UserModule.class, AuthModule.class})
public interface UserSubComponent {

    void inject(UserProfileActivity userProfileActivity);
}
