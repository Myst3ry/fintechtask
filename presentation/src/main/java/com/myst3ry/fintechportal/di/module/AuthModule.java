package com.myst3ry.fintechportal.di.module;

import com.myst3ry.data.repository.AuthRepositoryImpl;
import com.myst3ry.data.source.remote.api.ApiMapper;
import com.myst3ry.domain.interactor.AuthInteractor;
import com.myst3ry.domain.interactor.AuthInteractorImpl;
import com.myst3ry.domain.repository.AuthRepository;
import com.myst3ry.fintechportal.auth.AuthPresenter;
import com.myst3ry.fintechportal.di.scope.AuthScope;

import dagger.Module;
import dagger.Provides;

@Module
public class AuthModule {

    @Provides
    @AuthScope
    AuthPresenter providesAuthPresenter(final AuthInteractor authInteractor) {
        return new AuthPresenter(authInteractor);
    }

    @Provides
    @AuthScope
    AuthRepository providesAuthRepository(final ApiMapper apiMapper) {
        return new AuthRepositoryImpl(apiMapper);
    }

    @Provides
    @AuthScope
    AuthInteractor providesAuthInteractor(final AuthRepository authRepository) {
        return new AuthInteractorImpl(authRepository);
    }
}
