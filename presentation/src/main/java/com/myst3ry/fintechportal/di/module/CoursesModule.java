package com.myst3ry.fintechportal.di.module;

import com.myst3ry.data.repository.CoursesRepositoryImpl;
import com.myst3ry.data.source.local.cache.Cache;
import com.myst3ry.data.source.remote.api.ApiMapper;
import com.myst3ry.domain.interactor.CoursesInteractor;
import com.myst3ry.domain.interactor.CoursesInteractorImpl;
import com.myst3ry.domain.repository.CoursesRepository;
import com.myst3ry.fintechportal.courses.CoursesPresenter;
import com.myst3ry.fintechportal.di.scope.CoursesScope;

import dagger.Module;
import dagger.Provides;

@Module
public class CoursesModule {

    @Provides
    @CoursesScope
    CoursesPresenter providesCoursesPresenter(final CoursesInteractor coursesInteractor) {
        return new CoursesPresenter(coursesInteractor);
    }

    @Provides
    @CoursesScope
    CoursesRepository providesCoursesRepository(final ApiMapper apiMapper, final Cache cache) {
        return new CoursesRepositoryImpl(apiMapper, cache);
    }

    @Provides
    @CoursesScope
    CoursesInteractor providesCoursesInteractor(final CoursesRepository coursesRepository) {
        return new CoursesInteractorImpl(coursesRepository);
    }
}
