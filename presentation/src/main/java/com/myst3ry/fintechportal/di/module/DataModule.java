package com.myst3ry.fintechportal.di.module;

import com.myst3ry.data.source.local.cache.Cache;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    @Provides
    @Singleton
    Cache providesCache() {
        return new Cache();
    }
}
