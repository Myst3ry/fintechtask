package com.myst3ry.fintechportal.di.module;

import com.myst3ry.data.repository.EventsRepositoryImpl;
import com.myst3ry.data.source.local.cache.Cache;
import com.myst3ry.data.source.remote.api.ApiMapper;
import com.myst3ry.domain.interactor.EventsInteractor;
import com.myst3ry.domain.interactor.EventsInteractorImpl;
import com.myst3ry.domain.repository.EventsRepository;
import com.myst3ry.fintechportal.di.scope.EventsScope;
import com.myst3ry.fintechportal.events.EventsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class EventsModule {

    @Provides
    @EventsScope
    EventsPresenter providesEventsPresenter(final EventsInteractor eventsInteractor) {
        return new EventsPresenter(eventsInteractor);
    }

    @Provides
    @EventsScope
    EventsRepository providesEventsRepository(final ApiMapper apiMapper, final Cache cache) {
        return new EventsRepositoryImpl(apiMapper, cache);
    }

    @Provides
    @EventsScope
    EventsInteractor providesEventsInteractor(final EventsRepository eventsRepository) {
        return new EventsInteractorImpl(eventsRepository);
    }
}
