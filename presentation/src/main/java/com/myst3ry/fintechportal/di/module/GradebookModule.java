package com.myst3ry.fintechportal.di.module;

import com.myst3ry.data.repository.GradebookRepositoryImpl;
import com.myst3ry.data.source.local.cache.Cache;
import com.myst3ry.data.source.remote.api.ApiMapper;
import com.myst3ry.domain.interactor.GradebookInteractor;
import com.myst3ry.domain.interactor.GradebookInteractorImpl;
import com.myst3ry.domain.repository.GradebookRepository;
import com.myst3ry.fintechportal.courses.gradebook.GradebookPresenter;
import com.myst3ry.fintechportal.di.scope.CoursesScope;

import dagger.Module;
import dagger.Provides;

@Module
public class GradebookModule {

    @Provides
    @CoursesScope
    GradebookPresenter providesGradebookPresenter(final GradebookInteractor gradebookInteractor) {
        return new GradebookPresenter(gradebookInteractor);
    }

    @Provides
    @CoursesScope
    GradebookRepository providesGradebookRepository(final ApiMapper apiMapper, final Cache cache) {
        return new GradebookRepositoryImpl(apiMapper, cache);
    }

    @Provides
    @CoursesScope
    GradebookInteractor providesGradebookInteractor(final GradebookRepository gradebookRepository) {
        return new GradebookInteractorImpl(gradebookRepository);
    }
}
