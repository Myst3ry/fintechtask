package com.myst3ry.fintechportal.di.module;

import com.myst3ry.data.repository.UserRepositoryImpl;
import com.myst3ry.data.source.local.cache.Cache;
import com.myst3ry.data.source.remote.api.ApiMapper;

import com.myst3ry.domain.interactor.UserInteractor;
import com.myst3ry.domain.interactor.UserInteractorImpl;

import com.myst3ry.domain.repository.UserRepository;

import com.myst3ry.fintechportal.di.scope.UserScope;
import com.myst3ry.fintechportal.user.UserProfilePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {

    @Provides
    @UserScope
    UserProfilePresenter providesUserProfilePresenter(final UserInteractor userInteractor) {
        return new UserProfilePresenter(userInteractor);
    }

    @Provides
    @UserScope
    UserRepository providesUserRepository(final ApiMapper apiMapper, final Cache cache) {
        return new UserRepositoryImpl(apiMapper, cache);
    }

    @Provides
    @UserScope
    UserInteractor providesUserInteractor(final UserRepository userRepository) {
        return new UserInteractorImpl(userRepository);
    }
}
