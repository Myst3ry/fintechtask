package com.myst3ry.fintechportal.events;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.myst3ry.domain.model.Event;
import com.myst3ry.fintechportal.FintechPortalApp;
import com.myst3ry.fintechportal.R;
import com.myst3ry.fintechportal.auth.AuthActivity;
import com.myst3ry.fintechportal.base.BaseActivity;
import com.myst3ry.fintechportal.courses.CoursesActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class EventsActivity extends BaseActivity implements EventsView {

    @BindView(R.id.tv_category_active_events_title)
    TextView activeCategoryTextView;
    @BindView(R.id.tv_category_archive_events_title)
    TextView archiveCategoryTextView;
    @BindView(R.id.rv_active_events)
    RecyclerView activeRecyclerView;
    @BindView(R.id.rv_archive_events)
    RecyclerView archiveRecyclerView;
    @BindView(R.id.tv_empty_active_events)
    TextView activeEmptyTextView;
    @BindView(R.id.tv_empty_archive_events)
    TextView archiveEmptyTextView;
    @BindView(R.id.refresher)
    SwipeRefreshLayout swipeRefreshLayout;

    @Inject
    EventsPresenter eventsPresenter;

    private EventsAdapter activeEventsAdapter;
    private EventsAdapter archiveEventsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        FintechPortalApp.get(this).getAppComponent().getEventsSubComponent().inject(this);
        eventsPresenter.attachView(this);

        initAdapters();
        prepareActiveEventsRecyclerView();
        prepareArchiveEventsRecyclerView();
        setSwipeRefreshListener();
        setCategoryListeners();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_events, menu);
        MenuItem itemSignIn = menu.findItem(R.id.action_sign_in);
        MenuItem itemMyCourses = menu.findItem(R.id.action_my_courses);
        itemSignIn.setVisible(!isSignIn());
        itemMyCourses.setVisible(isSignIn());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_sign_in:
                onSignInClick();
                return true;
            case R.id.action_my_courses:
                onMyCoursesClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        getCurrentEvents();
    }

    @Override
    public void setActiveEvents(final List<Event> events) {
        activeEventsAdapter.setEvents(events);
    }

    @Override
    public void setArchiveEvents(final List<Event> events) {
        archiveEventsAdapter.setEvents(events);
    }

    @Override
    public void showActiveEmptyText() {
        if (activeEmptyTextView != null && activeEmptyTextView.getVisibility() == View.GONE) {
            activeRecyclerView.setVisibility(View.GONE);
            activeEmptyTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showArchiveEmptyText() {
        if (archiveEmptyTextView != null && archiveEmptyTextView.getVisibility() == View.GONE) {
            archiveRecyclerView.setVisibility(View.GONE);
            archiveEmptyTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideActiveEmptyText() {
        if (activeEmptyTextView != null && activeEmptyTextView.getVisibility() == View.VISIBLE) {
            activeEmptyTextView.setVisibility(View.GONE);
            activeRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideArchiveEmptyText() {
        if (archiveEmptyTextView != null && archiveEmptyTextView.getVisibility() == View.VISIBLE) {
            archiveEmptyTextView.setVisibility(View.GONE);
            archiveRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideSwipeRefreshLayout() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void showError(String errorText) {
        Toast.makeText(this, errorText, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(isDestroyed()) {
            eventsPresenter.detachView();
            eventsPresenter.disposeAll();
        }
    }

    private boolean isSignIn() {
        return !getSharedPreferences("CookiePersistence", MODE_PRIVATE).getAll().isEmpty();
    }

    private void onSignInClick() {
        startActivity(AuthActivity.newIntent(this));
    }

    private void onMyCoursesClick() {
        startActivity(CoursesActivity.newIntent(this));
    }

    private void getCurrentEvents() {
        eventsPresenter.getEvents();
    }

    private void setCategoryListeners() {
        activeCategoryTextView.setOnClickListener(v -> {
            if (activeRecyclerView.getVisibility() == View.GONE) {
                activeRecyclerView.setVisibility(View.VISIBLE);
            } else {
                activeRecyclerView.setVisibility(View.GONE);
            }
        });

        archiveCategoryTextView.setOnClickListener(v -> {
            if (archiveRecyclerView.getVisibility() == View.GONE) {
                archiveRecyclerView.setVisibility(View.VISIBLE);
            } else {
                archiveRecyclerView.setVisibility(View.GONE);
            }
        });
    }

    private void initAdapters() {
        activeEventsAdapter = new EventsAdapter();
        archiveEventsAdapter = new EventsAdapter();
    }

    private void prepareActiveEventsRecyclerView() {
        activeRecyclerView.setAdapter(activeEventsAdapter);
        activeRecyclerView.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
    }

    private void prepareArchiveEventsRecyclerView() {
        archiveRecyclerView.setAdapter(archiveEventsAdapter);
        archiveRecyclerView.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
    }

    private void setSwipeRefreshListener() {
        swipeRefreshLayout.setOnRefreshListener(this::getCurrentEvents);
    }
}
