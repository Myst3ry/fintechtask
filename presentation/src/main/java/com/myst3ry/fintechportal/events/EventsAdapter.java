package com.myst3ry.fintechportal.events;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myst3ry.domain.model.Event;
import com.myst3ry.fintechportal.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventHolder> {

    private List<Event> events;

    public EventsAdapter() {
        this.events = new ArrayList<>();
    }

    @NonNull
    @Override
    public EventHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        return new EventHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_event, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final EventHolder holder, final int position) {
        holder.bind(getEvent(position));
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public void setEvents(final List<Event> events) {
        this.events = events;
        notifyDataSetChanged();
    }

    private Event getEvent(final int position) {
        return events.get(position);
    }

    final class EventHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_event_title)
        TextView eventTitleTextView;
        @BindView(R.id.tv_event_date)
        TextView eventDateTextView;
        @BindView(R.id.tv_event_place)
        TextView eventPlaceTextView;
        @BindView(R.id.tv_event_description)
        TextView eventDescriptionTextView;

        EventHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(final Event currentEvent) {
            eventDateTextView.setText(currentEvent.getDate());
            eventTitleTextView.setText(currentEvent.getTitle());
            eventPlaceTextView.setText(currentEvent.getPlace());
            eventDescriptionTextView.setText(currentEvent.getDescription());
        }
    }
}
