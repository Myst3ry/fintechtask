package com.myst3ry.fintechportal.events;

import com.myst3ry.domain.interactor.EventsInteractor;
import com.myst3ry.domain.model.Event;
import com.myst3ry.fintechportal.base.BasePresenter;
import com.myst3ry.fintechportal.di.scope.EventsScope;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@EventsScope
public class EventsPresenter extends BasePresenter<EventsView> {

    private final EventsInteractor eventsInteractor;
    private final List<Event> activeEvents;

    public EventsPresenter(final EventsInteractor eventsInteractor) {
        this.eventsInteractor = eventsInteractor;
        this.activeEvents = new ArrayList<>();
    }

    public void getEvents() {
        addDisposable(eventsInteractor.getActiveEvents()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .flatMap(events -> {
                    activeEvents.addAll(events);
                    return eventsInteractor.getArchiveEvents()
                            .subscribeOn(Schedulers.io());
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onError)
        );
    }

    private void onSuccess(final List<Event> archiveEvents) {
        getView().hideSwipeRefreshLayout();
        if (!activeEvents.isEmpty()) {
            getView().hideActiveEmptyText();
            getView().setActiveEvents(activeEvents);
        } else {
            getView().showActiveEmptyText();
        }
        if (!archiveEvents.isEmpty()) {
            getView().hideArchiveEmptyText();
            getView().setArchiveEvents(archiveEvents);
        } else {
            getView().showArchiveEmptyText();
        }
    }

    private void onError(final Throwable throwable) {
        getView().hideSwipeRefreshLayout();
        getView().showError(throwable.getLocalizedMessage());
    }


}
