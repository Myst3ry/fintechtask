package com.myst3ry.fintechportal.events;

import com.myst3ry.domain.model.Event;
import com.myst3ry.fintechportal.base.BaseView;

import java.util.List;

public interface EventsView extends BaseView {

    void setActiveEvents(final List<Event> events);

    void setArchiveEvents(final List<Event> events);

    void showActiveEmptyText();

    void showArchiveEmptyText();

    void hideActiveEmptyText();

    void hideArchiveEmptyText();

    void hideSwipeRefreshLayout();

    void showError(final String errorText);

}
