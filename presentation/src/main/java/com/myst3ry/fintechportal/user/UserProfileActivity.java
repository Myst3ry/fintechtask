package com.myst3ry.fintechportal.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.myst3ry.domain.model.User;
import com.myst3ry.fintechportal.FPGlideModule;
import com.myst3ry.fintechportal.FintechPortalApp;
import com.myst3ry.fintechportal.GlideApp;
import com.myst3ry.fintechportal.R;
import com.myst3ry.fintechportal.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

public class UserProfileActivity extends BaseActivity implements UserProfileView {

    @BindView(R.id.iv_user_avatar)
    ImageView avatarImageView;
    @BindView(R.id.tv_user_last_name)
    TextView lastNameTextView;
    @BindView(R.id.tv_user_first_name)
    TextView firstNameTextView;
    @BindView(R.id.tv_user_middle_name)
    TextView middleNameTextView;
    @BindView(R.id.tv_user_birthday)
    TextView birthdayTextView;
    @BindView(R.id.tv_user_email)
    TextView emailTextView;
    @BindView(R.id.tv_user_region)
    TextView regionTextView;
    @BindView(R.id.tv_user_university)
    TextView universityTextView;
    @BindView(R.id.tv_user_faculty)
    TextView facultyTextView;
    @BindView(R.id.tv_user_department)
    TextView departmentTextView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @Inject
    UserProfilePresenter userProfilePresenter;

    public static Intent newIntent(Context context) {
        return new Intent(context, UserProfileActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        FintechPortalApp.get(this).getAppComponent().getUserSubComponent().inject(this);
        userProfilePresenter.attachView(this);
        prepareActionBar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getUserProfile();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_sign_out:
                //onSignOutClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setUserInfo(final User user) {
        loadUserAvatar(user.getAvatar());
        lastNameTextView.setText(user.getLastName());
        firstNameTextView.setText(user.getFirstName());
        middleNameTextView.setText(user.getMiddleName());
        birthdayTextView.setText(user.getBirthday());
        emailTextView.setText(user.getEmail());
        regionTextView.setText(user.getRegion());
        universityTextView.setText(user.getUniversity());
        facultyTextView.setText(user.getFaculty());
        departmentTextView.setText(user.getDepartment());
    }

    @Override
    public void showProgressBar() {
        if (progressBar != null && progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgressBar() {
        if (progressBar != null && progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError(String errorText) {
        Toast.makeText(this, errorText, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(isDestroyed()) {
            userProfilePresenter.detachView();
            userProfilePresenter.disposeAll();
        }
    }

    private void getUserProfile() {
        userProfilePresenter.getUserProfile();
    }

    private void loadUserAvatar(final String url) {
        Timber.w(url);
        GlideApp.with(this)
                .load(url)
                .centerInside()
                .transition(DrawableTransitionOptions.withCrossFade(FPGlideModule.CROSS_FADE_DURATION))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(avatarImageView);
    }

    private void prepareActionBar() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
