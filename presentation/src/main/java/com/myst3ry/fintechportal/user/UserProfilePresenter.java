package com.myst3ry.fintechportal.user;

import com.myst3ry.domain.interactor.UserInteractor;
import com.myst3ry.domain.model.User;
import com.myst3ry.fintechportal.base.BasePresenter;
import com.myst3ry.fintechportal.di.scope.UserScope;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@UserScope
public class UserProfilePresenter extends BasePresenter<UserProfileView> {

    private final UserInteractor userInteractor;

    public UserProfilePresenter(final UserInteractor userInteractor) {
        this.userInteractor = userInteractor;
    }

    public void getUserProfile() {
        getView().showProgressBar();
        addDisposable(userInteractor.getUserProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onError)
        );
    }

    private void onSuccess(final User user) {
        if (user != null) {
            getView().hideProgressBar();
            getView().setUserInfo(user);
        }
    }

    private void onError(final Throwable throwable) {
        getView().hideProgressBar();
        getView().showError(throwable.getLocalizedMessage());
    }
}
