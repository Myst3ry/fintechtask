package com.myst3ry.fintechportal.user;

import com.myst3ry.domain.model.User;
import com.myst3ry.fintechportal.base.BaseView;

public interface UserProfileView extends BaseView {

    void setUserInfo(final User user);

    void showProgressBar();

    void hideProgressBar();

    void showError(final String errorText);
}
